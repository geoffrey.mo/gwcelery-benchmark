import argparse
import json
from datetime import datetime

from ligo.gracedb.rest import GraceDb
from gwpy.time import tconvert

parser = argparse.ArgumentParser()
parser.add_argument('weeks')
parser.add_argument('--gid', action="store_true")
parser.add_argument('--plot', action="store_true")
parser.add_argument('--log', action="store_true")
parser.add_argument('--xlog', action="store_true")
parser.add_argument('--print', action="store_true")
parser.add_argument('--eventtime', action="store_true")
parser.add_argument('--pipeline', action="store_true")
args = parser.parse_args()
weeks = args.weeks
print_id = args.gid
plot = args.plot
log = args.log
xlog = args.xlog
prnt = args.print
if args.eventtime:
    begintime = 'event'
elif args.pipeline:
    begintime = 'pipeline'
else:
    begintime = 'se'

playground = "https://gracedb-playground.ligo.org/api/"
today = datetime.now().strftime("%Y-%m-%d")
# Initialize GraceDb client
client = GraceDb(playground)


def find_gcn_time(se_log):
    filtered = filter(
        lambda entry: entry['comment'] == 'Added label: GCN_PRELIM_SENT',
        se_log
    )
    try:
        return dict(list(filtered)[0])['created']
    except IndexError:
        return 'no gcn sent'

def find_creation_time(se_log):
    return list(se_log)[0]['created']

def find_event_time(se_log):
    # this returns the gpstime of the preferred event. Need to figure out how
    # to get gcn_sent time in the same coordinate system
    return list(se_log)[0]['t_0']

def to_gpstime(time):
    return float(tconvert(time))

def to_datetime(time):
    return datetime.strptime(time, '%Y-%m-%d %H:%M:%S %Z')

def get_deltaTs(weeks, begintime='se'):
    superevent_range = "created: {} weeks ago .. now".format(weeks)
    # Get superevents
    superevents = client.superevents(superevent_range)
    # Superevent IDs
    se_ids = [se['superevent_id'] for se in superevents]
    # List of dicts for all the superevents
    # se_logs = [json.loads(client.logs(se_id).read().decode('utf-8'))['log']
    #           for se_id in se_ids]

    deltaT_dict = {}
    for se_id in se_ids:
        se_log = json.loads(client.logs(se_id).read().decode('utf-8'))['log']
        gcn_time_utc = find_gcn_time(se_log)
        creation_time_utc = find_creation_time(se_log)
        event_time = json.loads(
            client.superevent(se_id).read().decode('utf-8'))['t_0']
        try:
            gcn_time = to_datetime(gcn_time_utc)
            creation_time = to_datetime(creation_time_utc)
            if begintime == 'se':
                dt = getattr(gcn_time - creation_time, 'seconds')
            if begintime == 'pipeline':
                dt = to_gpstime(creation_time_utc) - event_time
            if begintime == 'event':
                dt = to_gpstime(gcn_time_utc) - event_time
            deltaT_dict.update({se_id: dt})
        except ValueError:
            pass
    if print_id:
        return deltaT_dict
    else:
        return list(deltaT_dict.values())

def histogram(times, weeks):
    import matplotlib.pyplot as plt
    import numpy as np
    mean = 'Mean: {0:.2f}'.format(np.mean(times))
    median = 'Median: {0:.2f}'.format(np.median(times))
    stdev = 'StDev: {0:.2f}'.format(np.std(times))
    total = 'Events: {}'.format(len(times))
    fig, ax = plt.subplots()
    ax.hist(times, int(max(times)//30+1))
    ax.annotate('{}\n{}\n{}\n{}'.format(mean, stdev, median, total), xy=(0.85,0.85),
                xycoords='figure fraction', ha='right', va='top',
                bbox=dict(boxstyle='round', fc='w'))
    plt.ylabel('Number of superevents')
    if log:
        plt.yscale('log')
    if xlog:
        plt.xscale('log')
    if begintime == 'se':
        plt.xlabel('Seconds after superevent creation')
        plt.title(
            'Time for GCNs to be sent after superevent creation\n ({} weeks before {})'.format(
                weeks, today))
    if begintime == 'event':
        plt.xlabel('Seconds after event')
        plt.title(
            'Time for GCNs to be sent after event\n ({} weeks before {})'.format(
                weeks, today))
    if begintime == 'pipeline':
        plt.xlabel('Seconds after event')
        plt.title(
            'Time for superevents to be created after event time\n ({} weeks before {})'.format(
                weeks, today))
    plt.savefig(
        '/home/geoffrey.mo/public_html/gwcelery-benchmark/benchmark-{}-{}-wk.png'.format(begintime, weeks), dpi=300
    )

dts = get_deltaTs(weeks, begintime)
if plot:
    histogram(list(dts.values()), weeks)
if prnt:
    print(dts)
